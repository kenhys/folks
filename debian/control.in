Source: folks
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 12),
               dh-sequence-gnome,
               evolution-data-server-dev (>= 3.33.2),
               gir1.2-telepathyglib-0.12,
               gobject-introspection (>= 1.31.0-2~),
               libdbus-glib-1-dev,
               libebook-contacts1.2-dev (>= 3.33.2),
               libebook1.2-dev (>= 3.33.2),
               libedataserver1.2-dev (>= 3.33.2),
               libgee-0.8-dev (>= 0.8.4),
               libgirepository1.0-dev (>= 1.30),
               libglib2.0-dev (>= 2.44),
               libreadline-dev,
               libtelepathy-glib-dev (>= 0.19.9),
               libxml2-dev,
               meson (>= 0.51),
               python3,
               python3-dbusmock,
               valac (>= 0.22)
Build-Depends-Indep: gtk-doc-tools <!nodoc>, valadoc <!nodoc>
Rules-Requires-Root: no
Standards-Version: 4.5.0
Section: libs
Homepage: https://wiki.gnome.org/Projects/Folks
Vcs-Git: https://salsa.debian.org/gnome-team/folks.git
Vcs-Browser: https://salsa.debian.org/gnome-team/folks

Package: libfolks25
Architecture: any
Depends: folks-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libfolks-eds25
Breaks: libfolks-eds25 (<< 0.8.0-2~),
        libfolks-telepathy25 (<< 0.8.0-2~)
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: library to aggregates people into metacontacts
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.

Package: libfolks-dev
Section: libdevel
Architecture: any
Depends: gir1.2-folks-0.6 (= ${binary:Version}),
         libfolks25 (= ${binary:Version}),
         libgee-0.8-dev,
         libglib2.0-dev (>= 2.40),
         ${misc:Depends}
Multi-Arch: same
Description: library to aggregates people into metacontact - development files
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the development files for libfolks library.

Package: libfolks-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Build-Profiles: <!nodoc>
Multi-arch: foreign
Description: library to aggregates people into metacontact - documentation
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the documentation for libfolks library.

Package: folks-common
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: library to aggregates people into metacontacts (common files)
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains common files (translations) used by folks components.

Package: gir1.2-folks-0.6
Architecture: any
Depends: ${gir:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Provides: gir1.2-folksdummy-0.6 (= ${binary:Version}),
          gir1.2-folkseds-0.6 (= ${binary:Version}),
          gir1.2-folkstelepathy-0.6 (= ${binary:Version})
Multi-Arch: same
Section: introspection
Description: library to aggregates people into metacontacts - GObject-Introspection
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 It can be used by packages using the GIRepository format to generate dynamic
 bindings.

Package: libfolks-telepathy25
Architecture: any
Depends: libfolks25 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: folks-common (= ${source:Version})
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: Telepathy backend for libfolks
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the Telepathy backend for libfolks

Package: libfolks-telepathy-dev
Section: libdevel
Architecture: any
Depends: libfolks-dev (= ${binary:Version}),
         libfolks-telepathy25 (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
         libtelepathy-glib-dev (>= 0.19.9),
         ${misc:Depends}
Multi-Arch: same
Description: Telepathy backend for libfolks - development files
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the development files for libfolks telepathy backend.

Package: libfolks-telepathy-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Build-Profiles: <!nodoc>
Multi-arch: foreign
Description: Telepathy backend for libfolks - documentation
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the documentation for libfolks telepathy backend.

Package: libfolks-eds25
Architecture: any
Depends: evolution-data-server (>= 3.2.0),
         libfolks25 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: folks-common (= ${source:Version})
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: Evolution-data-server backend for libfolks
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the EDS backend for libfolks

Package: libfolks-eds-dev
Section: libdevel
Architecture: any
Depends: libebook1.2-dev (>= 3.13.90),
         libedataserver1.2-dev (>= 3.13.90),
         libfolks-dev (= ${binary:Version}),
         libfolks-eds25 (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
         ${misc:Depends}
Multi-Arch: same
Description: Evolution-data-server backend for libfolks - development files
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the development files for libfolks EDS backend.

Package: libfolks-eds-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Build-Profiles: <!nodoc>
Multi-arch: foreign
Description: Evolution-data-server backend for libfolks - documentation
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the documentation for libfolks EDS backend.

Package: libfolks-dummy25
Architecture: any
Depends: libfolks25 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: folks-common (= ${source:Version})
Pre-Depends: ${misc:Pre-Depends}
Breaks: libfolks25 (<< 0.10.0)
Replaces: libfolks25 (<< 0.10.0)
Multi-Arch: same
Description: Dummy backend for libfolks
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the dummy backend for libfolks

Package: libfolks-dummy-dev
Section: libdevel
Architecture: any
Depends: libfolks-dev (= ${binary:Version}),
         libfolks-dummy25 (= ${binary:Version}),
         libfolks-eds25 (= ${binary:Version}),
         libglib2.0-dev (>= 2.40),
         ${misc:Depends}
Multi-Arch: same
Description: Dummy backend for libfolks - development files
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the development files for libfolks dummy backend.

Package: libfolks-dummy-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Build-Profiles: <!nodoc>
Multi-arch: foreign
Description: Dummy backend for libfolks - documentation
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains the documentation for libfolks dummy backend.

Package: folks-tools
Section: utils
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: folks-common (= ${source:Version})
Description: Telepathy backend for libfolks - database and import tools
 libfolks is a library that aggregates people from multiple sources
 (eg, Telepathy connection managers and eventually evolution data server,
 Facebook, etc.) to create metacontacts.
 .
 This package contains tools to inspect the folks database and import
 metacontacts from pidgin
